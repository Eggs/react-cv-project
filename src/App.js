import React, { Component } from "react";

import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import WorkIcon from "@material-ui/icons/Work";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import GradeIcon from "@material-ui/icons/Grade";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";

import RenderList from "./components/RenderList";
import RenderForm from "./components/RenderForm";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      general: [],
      education: [],
      work: [],
      isEdit: false,
    };

    this.submitExperience = this.submitExperience.bind(this);
    this.onEditClick = this.onEditClick.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  submitExperience(e, data, section) {
    e.preventDefault();
    let updateInfo = [];
    switch (section) {
      case "work":
        updateInfo = [...this.state.work];
        break;
      case "general":
        updateInfo = [...this.state.general];
        break;
      case "education":
        updateInfo = [...this.state.education];
        break;

      default:
        break;
    }
    // updateInfo is an array of Objects
    updateInfo.push(data[section]);
    this.setState({
      [section]: updateInfo,
    });
  }

  handleEdit(e, data, section, index) {
    e.preventDefault();
    let updateInfo = [];
    switch (section) {
      case "work":
        updateInfo = [...this.state.work];
        break;
      case "general":
        updateInfo = [...this.state.general];
        break;
      case "education":
        updateInfo = [...this.state.education];
        break;

      default:
        break;
    }

    updateInfo[index] = Object.assign(updateInfo[index],updateInfo[index],data[section]);
    this.setState({
      [section]: updateInfo,
      isEdit: false,
    });
    //console.log(updateInfo);
  }

  onEditClick(e) {
    this.setState({ isEdit: !this.state.isEdit });
  }

  render() {
    const workInfo = this.state.work;
    const generalInfo = this.state.general;
    const educationInfo = this.state.education;

    return (
      <div className="App">
        <Grid container spacing={2} direction="row">
          <Grid item>
            <Grid container spacing={2} direction="column">
              <RenderForm submitExperience={this.submitExperience} edits={this.state.general} />
            </Grid>
          </Grid>
          <Grid item xs>
            <Grid container>
              <Grid item xs={12}>
                <Paper elevation={3}>
                  <List>
                    <Typography variant="h1"> C.V.</Typography>
                    <ListItem>
                      <AccountBoxIcon />
                      <Typography variant="h6">Personal:</Typography>
                    </ListItem>
                    <ListItem>
                      {generalInfo.map((info, index) => {
                        return (
                          <RenderList
                            key={index}
                            info={info}
                            index={index}
                            handleEdit={this.handleEdit}
                            onEditClick={this.onEditClick}
                            isEdit={this.state.isEdit}
                            formId={"general"}
                            submitExperience={this.submitExperience}
                          />
                        );
                      })}
                    </ListItem>

                    <ListItem>
                      <GradeIcon />
                      <Typography variant="h6">Education:</Typography>
                    </ListItem>
                    <ListItem>
                      {educationInfo.map((info, index) => {
                        return (
                          <RenderList
                            key={index}
                            info={info}
                            index={index}
                            handleEdit={this.handleEdit}
                            onEditClick={this.onEditClick}
                            isEdit={this.state.isEdit}
                            formId={"education"}
                            submitExperience={this.submitExperience}
                          />
                        );
                      })}
                    </ListItem>

                    <ListItem>
                      <WorkIcon />
                      <Typography variant="h6"> Work:</Typography>
                    </ListItem>

                    <ListItem>
                      {workInfo.map((info, index) => {
                        return (
                          <RenderList
                            key={index}
                            info={info}
                            index={index}
                            handleEdit={this.handleEdit}
                            onEditClick={this.onEditClick}
                            isEdit={this.state.isEdit}
                            formId={"work"}
                            submitExperience={this.submitExperience}
                          />
                        );
                      })}
                    </ListItem>
                  </List>
                </Paper>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default App;
