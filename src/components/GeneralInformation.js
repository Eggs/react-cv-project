import React, { Component } from "react";

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import "./GeneralInformation.css";

class GeneralInformation extends Component {
  constructor(props) {
    super(props);
    this.state = { ...this.props.edits };
  }

  render() {
    console.log(this.state);
    return (
      <form id="general" onSubmit={this.props.onSubmit}>
        <Typography color="textSecondary" gutterBottom>
          Personal Details
        </Typography>
        <TextField
          id="standard-basic"
          label="First Name"
          name="First Name"
          onChange={this.props.handleChange}
          value={this.state["First Name"]}
        />
        <TextField id="standard-basic" label="Last Name" name="Last Name" onChange={this.props.handleChange} />
        <TextField id="standard-email" label="Email Address" name="Email Address" onChange={this.props.handleChange} />
        <TextField
          id="standard-number"
          label="Telephone Number"
          name="Telephone Number"
          onChange={this.props.handleChange}
        />

        <Button variant="contained" color="primary" type="submit">
          Done
        </Button>
      </form>
    );
  }
}

GeneralInformation.defaultProps = {
  itemToEdit: { "First Name": "" },
};

export default GeneralInformation;
