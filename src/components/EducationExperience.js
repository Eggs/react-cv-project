import React, { Component } from "react";

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";

import "date-fns";

class EducationExperience extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <form id="education" onSubmit={this.props.onSubmit}>
        <Typography color="textSecondary" gutterBottom>
          Education
        </Typography>
        <TextField id="standard-basic" label="School name" name="School Name" onChange={this.props.handleChange} />
        <TextField id="standard-basic" label="Subject name" name="Subject Name" onChange={this.props.handleChange} />
        <TextField
          id="standard-email"
          label="Level of study"
          name="Level of Study"
          onChange={this.props.handleChange}
        />

        <Grid container justify="space-around">
          <TextField
            id="date"
            label="Date awarded"
            type="date"
            defaultValue="2017-05-24"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>

        <Button variant="contained" color="primary" type="submit">
          Done
        </Button>
      </form>
    );
  }
}

export default EducationExperience;
