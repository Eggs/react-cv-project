import React, { Component } from "react";

import GeneralInformation from "./GeneralInformation";
import EducationExperience from "./EducationExperience";
import WorkExperience from "./WorkExperience";

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import List from "@material-ui/core/List";

import Divider from "@material-ui/core/Divider";

class RenderForm extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {};
  }

  handleChange(e) {
    const value = e.target.value;
    const formName = e.target.parentNode.parentNode.parentNode.id;
    this.setState({
      [formName]: {
        ...this.state[formName],
        [e.target.name]: value,
      },
    });
    console.log(this.state);
  }

  onSubmit(e) {
    e.preventDefault();
    const currentState = this.state;
    this.props.submitExperience(e, currentState, e.target.id); // Gets the ID of the form being submitted.

    // Reset the form and state so we don't get duplicate entries.
    e.target.reset();
    this.setState({ work: {}, general: {}, personal: {} });
  }

  render() {
    return (
      <Card className="root">
        <CardContent>
          <List>
            <GeneralInformation
              onSubmit={this.onSubmit}
              handleChange={this.handleChange}
              edits={this.props.edits}
              formFields={this.state.general}
            />
          </List>
          <Divider />
          <List>
            <EducationExperience onSubmit={this.onSubmit} handleChange={this.handleChange} />
          </List>
          <Divider />
          <List>
            <WorkExperience onSubmit={this.onSubmit} handleChange={this.handleChange} />
          </List>
        </CardContent>
      </Card>
    );
  }
}

export default RenderForm;
