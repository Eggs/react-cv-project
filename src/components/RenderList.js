import React, { Component } from "react";

import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import EditIcon from "@material-ui/icons/Edit";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

class RenderList extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state= {};
  }

  handleChange(e) {
    const value = e.target.value;
    const formName = e.target.parentNode.parentNode.parentNode.parentNode.id;
    this.setState({
      [formName]: {
        ...this.state[formName],
        [e.target.name]: value,
      },
    });
    console.log(this.state);
  }

  onSubmit(e) {
    e.preventDefault();
    const currentState = this.state;
    this.props.handleEdit(e, currentState, e.target.id, e.target.getAttribute("index"));

    // Reset the form and state so we don't get duplicate entries.
    e.target.reset();
    this.setState({ work: {}, general: {}, personal: {} });
  }

  createList(list) {
    const { isEdit, handleEdit, formId } = this.props;
    const content = [];
    let i = 0;

    for (const [key, value] of Object.entries(list)) {
      content.push(
        !isEdit ? (
          <div key={i}>
            <Typography variant="subtitle2" color="textSecondary">
              {key}
            </Typography>
            <Typography>{value}</Typography>
          </div>
        ) : (
          <div key={i}>
            <Typography variant="subtitle2" color="textSecondary">
              {key}
            </Typography>
            <TextField id={key} name={key} label={value} onChange={this.handleChange}/>
          </div>
        )
      );
      i++;
    }
    return content;
  }

  render() {
    const { info, index, onEditClick, isEdit, handleEdit, formId } = this.props;
    return (
      <Card key={index}>
        <CardContent>
          {!isEdit ? (
            this.createList(info)
          ) : (
            <form id={formId} index={index} onSubmit={this.onSubmit}>
              {this.createList(info)}
              <Button variant="contained" color="primary" type="submit">
                Done
              </Button>
            </form>
          )}
          <EditIcon id={"edit-item-" + index} key={index} onClick={onEditClick} />
        </CardContent>
      </Card>
    );
  }
}

export default RenderList;
